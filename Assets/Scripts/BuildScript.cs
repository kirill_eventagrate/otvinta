﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildScript : MonoBehaviour {


    public PinScript[] pinScripts;
    public Button PinBtn;

    [HideInInspector]
    public bool isLocalPinsShowing = false;

	// Use this for initialization
	void Start () {
        PinBtn.onClick.AddListener(() => OnBtnClick());
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    

    public void OnBtnClick()
    {
        Vector3 newPos = new Vector3(transform.position.x, transform.position.y + 1.2f, transform.position.z - 1.2f);
        CameraScript.instanse.Moving(newPos, 1.8f);
        if (UIManagerScript.instanse.LeftPanel.GetComponent<LeftPanel>().isPanelShow)
        {
            UIManagerScript.instanse.LeftPanel.GetComponent<LeftPanel>().ShowHidePanel();
            UIManagerScript.instanse.LeftPanel.GetComponent<LeftPanel>().wasShow = true;
        }
        MapScript.instanse.HidePins(1f);
        Loom.QueueOnMainThread(() => {
            ShowLocalPins(1f);
        }, 1.5f);
    }

    public void ShowPin(float time)
    {
        iTween.ScaleTo(gameObject, iTween.Hash("scale", Vector3.one, "time", time, "easetype", iTween.EaseType.easeOutQuint));
        HideLocalPins(time);
    }

    public void HidePin(float time)
    {
        iTween.ScaleTo(gameObject, iTween.Hash("scale", Vector3.zero, "time", time, "easetype", iTween.EaseType.easeOutQuint));
    }

    public void ShowLocalPins(float time)
    {
        foreach (PinScript pin in pinScripts)
        {
            pin.ShowPin(time);
        }
    }

    public void HideLocalPins(float time)
    {
        foreach (PinScript pin in pinScripts)
        {
            pin.HidePin(time);
        }
    }
}
