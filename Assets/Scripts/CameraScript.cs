﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    public static CameraScript instanse = null;

    List<Vector3> positionStack = new List<Vector3>();

    bool isAppStart = false;
    public bool canClick = false;
    //Vector3 startPosition;
    private void Awake()
    {
        if (instanse == null)
            instanse = this;
    }

    // Use this for initialization
    void Start () {
        //positionStack.Add(transform.position);
        //startPosition = transform.position;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartApp()
    {
        if (!isAppStart)
        {
            GameObject startPosObj = GameObject.FindGameObjectWithTag("StartPos");
            if (startPosObj != null)
            {
                Moving(startPosObj.transform.position, 3f);
                //positionStack.Add(startPosObj.transform.position);
                Loom.QueueOnMainThread(() => {
                    MapScript.instanse.ShowPins(1f);
                }, 3.5f);
                isAppStart = true;
                canClick = true;
            }
        }
    }

    public void ResetApp()
    {
        isAppStart = false;
    }

    public void Moving(Vector3 newPos, float time)
    {
        positionStack.Add(transform.position);
        iTween.MoveTo(gameObject, iTween.Hash("position", newPos, "time", time, "easetype", iTween.EaseType.easeOutCubic));
    }

    public void MoveBack()
    {
        if (positionStack.Count > 1)
        {
            iTween.MoveTo(gameObject, iTween.Hash("position", positionStack[positionStack.Count - 1], "time", 1.8f, "easetype", iTween.EaseType.easeOutCubic));
            Loom.QueueOnMainThread(() => {
                
            }, 0.5f);
            MapScript.instanse.ShowPins(1f);
        }
        else if (positionStack.Count == 1)
        {
            if (UIManagerScript.instanse.LeftPanel.GetComponent<LeftPanel>().isPanelShow)
            {
                UIManagerScript.instanse.LeftPanel.GetComponent<LeftPanel>().ShowHidePanel();
            }
            iTween.MoveTo(gameObject, iTween.Hash("position", positionStack[0], "time", 1.8f, "easetype", iTween.EaseType.easeOutCubic));
            Loom.QueueOnMainThread(() => {
                UIManagerScript.instanse.BeginToShowStartScreen();
                MapScript.instanse.HidePins(1f);
            }, 1.2f);
        }
        positionStack.RemoveAt(positionStack.Count - 1);
    }
}
