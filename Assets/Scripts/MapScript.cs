﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapScript : MonoBehaviour {

    public static MapScript instanse = null;

    public BuildScript[] pins;

    private void Awake()
    {
        if (instanse == null)
            instanse = this;
    }

    // Use this for initialization
    void Start () {
        HidePins(0.1f);
        Loom.QueueOnMainThread(() => {
            gameObject.transform.localScale = Vector3.one;
        }, 2f);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowPins(float time)
    {
        foreach (BuildScript pin in pins)
        {
            pin.ShowPin(time);
        }
    }
    
    public void HidePins(float time)
    {
        foreach (BuildScript pin in pins)
        {
            pin.HidePin(time);
        }
    }
}
