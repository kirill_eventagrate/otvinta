﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class UIManagerScript : MonoBehaviour {

    public static UIManagerScript instanse = null;

    public Button BackBtn;
    public GameObject StartScreen;
    public GameObject PreviewScreen;
    public GameObject Popup;
    public Button StartBtn;
    public Sprite PlaySprite;
    public Sprite PauseSprite;

    public Text Title;
    public Text Desc;

    public Slider videoControllSlider;
    public Image SketchImg;
    public GameObject ImageItemsContainer;
    public GameObject ImageItemPrefab;
    public GameObject VideoItemPrefab;
    public Button PlayPauseBtn;
    public VideoPlayer BigVideoPlayer;
    public GameObject BigVideoPanel;
    public GameObject BlackPanelOfVideoPrev;
    public GameObject LeftPanel;


    
    Sprite[] Images;
    VideoClip videoClip;
    List<ImageItemScript> switcherImages = new List<ImageItemScript>();
    int typeOfPin = 0;

    private void Awake()
    {
        if (instanse == null)
            instanse = this;
    }

    // Use this for initialization
    void Start () {
        videoControllSlider.minValue = 0;
        Popup.transform.localScale = Vector3.zero;
        BackBtn.onClick.AddListener(() => CameraScript.instanse.MoveBack());
        StartBtn.onClick.AddListener(() => BeginToHideStartScreen());
        BigVideoPlayer.prepareCompleted += PreapreVideoParams;
        PreviewScreen.GetComponent<Button>().onClick.AddListener(() => BeginToHidePrevScreen());
    }
	
	// Update is called once per frame
	void Update () {
        if (BigVideoPlayer.isPlaying)
        {
            videoControllSlider.value = (float)BigVideoPlayer.time;
        }
    }

    
    // POPUP

    public void SetContent(string _title, string _desc, Sprite[] _images, VideoClip _videoClip)
    {
        Title.text = _title;
        Desc.text = _desc;
        Images = _images;
        videoClip = _videoClip;

        BigVideoPanel.SetActive(true);
        for (int i = 0; i < ImageItemsContainer.transform.childCount; i++)
        {
            Destroy(ImageItemsContainer.transform.GetChild(i).gameObject);
        }
        switcherImages.Clear();
        foreach (Sprite img in Images)
        {
            GameObject item = Instantiate(ImageItemPrefab) as GameObject;
            item.transform.SetParent(ImageItemsContainer.transform);
            item.GetComponent<RectTransform>().localScale = Vector3.one;
            item.GetComponent<ImageItemScript>().SetContent(img, SketchImg, BigVideoPanel);
            switcherImages.Add(item.GetComponent<ImageItemScript>());
        }

        if (_videoClip != null)
        {
            // for video -----
            GameObject videoItem = Instantiate(VideoItemPrefab) as GameObject;
            videoItem.transform.SetParent(ImageItemsContainer.transform);
            videoItem.GetComponent<RectTransform>().localScale = Vector3.one;
            videoItem.GetComponent<ImageItemScript>().SetContent(SketchImg, BigVideoPlayer, BigVideoPanel, _videoClip);
            switcherImages.Add(videoItem.GetComponent<ImageItemScript>());
            BigVideoPlayer.targetTexture.Release();
            BigVideoPlayer.clip = videoClip;
            BigVideoPlayer.time = 3;
            BigVideoPlayer.Prepare();
            // for video -----
        }


        switcherImages[0].ShowBorder();
        if (switcherImages.Count > 1)
        {
            SketchImg.gameObject.SetActive(true);
            SketchImg.sprite = Images[0];
            //BigVideoPanel.SetActive(false);
        }
        else if (switcherImages.Count == 1)
        {
            //BigVideoPanel.SetActive(true);
            SketchImg.gameObject.SetActive(false);
        }
        if (_videoClip == null)
        {
            SketchImg.gameObject.SetActive(true);
            SketchImg.sprite = Images[0];
        }
    }

    

    // BIG VIDEO
    void PreapreVideoParams(VideoPlayer _videoPlayer)
    {
        videoControllSlider.maxValue = CalculateLengh();
        PlayPauseBtn.gameObject.GetComponent<Image>().sprite = PlaySprite;
        _videoPlayer.audioOutputMode = VideoAudioOutputMode.None;
        _videoPlayer.Play();
        Loom.QueueOnMainThread(() => {
            _videoPlayer.Pause();
            _videoPlayer.time = 0;
            videoControllSlider.value = 0;
            _videoPlayer.audioOutputMode = VideoAudioOutputMode.Direct;
        }, 0.25f);

    }

    float CalculateLengh()
    {
        float fraction = (float)BigVideoPlayer.frameCount / (float)BigVideoPlayer.frameRate;
        return fraction;
    }

    public void MoveSlider()
    {
        BigVideoPlayer.time = videoControllSlider.value;
    }

    public void PlayPauseVideo()
    {
        if (BigVideoPlayer.isPlaying)
        {
            BigVideoPlayer.Pause();
            PlayPauseBtn.gameObject.GetComponent<Image>().sprite = PlaySprite;
        }
        else
        {
            BigVideoPlayer.Play();
            PlayPauseBtn.gameObject.GetComponent<Image>().sprite = PauseSprite;
        }
    }

    void ShowBlackPanel()
    {
        
    }

    // SHOW/HIDE POPUP
    public void ShowPopup(int _type)
    {
        typeOfPin = _type;
        iTween.ScaleTo(Popup, iTween.Hash("scale", Vector3.one, "time", .8f, "easetype", iTween.EaseType.easeOutBack));
    }

    public void HidePopup()
    {
        BigVideoPlayer.Pause();
        iTween.ScaleTo(Popup, iTween.Hash("scale", Vector3.zero, "time", .6f, "easetype", iTween.EaseType.easeInBack));
        Loom.QueueOnMainThread(() => {
            if (typeOfPin == 0)
            {
                CameraScript.instanse.MoveBack();
                Loom.QueueOnMainThread(() => {
                    MapScript.instanse.ShowPins(1f);
                }, 1.8f);
            }
            if (LeftPanel.GetComponent<LeftPanel>().wasShow)
            {
                LeftPanel.GetComponent<LeftPanel>().ShowHidePanel();
                LeftPanel.GetComponent<LeftPanel>().wasShow = false;
                /*Loom.QueueOnMainThread(() => {
                    MapScript.instanse.ShowPins(1f);
                }, 0.6f);*/
            }
            
        }, .6f);
    }


    // BIG IMAGE

    public void HideImageBorders()
    {
        foreach (ImageItemScript image in switcherImages)
        {
            image.HideBorder();
        }
    }

    

    // START SCREEN

    public void BeginToShowStartScreen()
    {
        StartCoroutine(ShowStartScreen());
    }

    void BeginToHideStartScreen()
    {
        iTween.ScaleTo(StartBtn.gameObject, iTween.Hash("scale", Vector3.zero, "time", .4f, "easetype", iTween.EaseType.easeInBack));
        BeginToShowPrevScreen();
        StartCoroutine(HideStartScreen());
    }

    IEnumerator HideStartScreen()
    {
        BeginToShowPrevScreen();
        yield return new WaitForSeconds(.6f);
        for (float i = 0; i < 1.1; i += 0.04f)
        {
            float transparent = Mathf.Lerp(1, 0, i);
            Color color = new Color(StartScreen.GetComponent<Image>().color.r, StartScreen.GetComponent<Image>().color.g, StartScreen.GetComponent<Image>().color.b, transparent);
            StartScreen.GetComponent<Image>().color = color;
            yield return new WaitForSeconds(0.01f);
        }
        StartScreen.SetActive(false);
        Invoke("BeginToHidePrevScreen", 5);
    }

    IEnumerator ShowStartScreen()
    {
        StartScreen.SetActive(true);
        for (float i = 0; i < 1.1; i += 0.04f)
        {
            float transparent = Mathf.Lerp(0, 1, i);
            Color color = new Color(StartScreen.GetComponent<Image>().color.r, StartScreen.GetComponent<Image>().color.g, StartScreen.GetComponent<Image>().color.b, transparent);
            StartScreen.GetComponent<Image>().color = color;
            yield return new WaitForSeconds(0.01f);
        }
        iTween.ScaleTo(StartBtn.gameObject, iTween.Hash("scale", Vector3.one, "time", .4f, "easetype", iTween.EaseType.easeOutBack));
        BeginToShowPrevScreen();
        CameraScript.instanse.ResetApp();
    }

    // PREVIEW SCREEN

    public void BeginToShowPrevScreen()
    {
        //StartCoroutine(ShowStartScreen());
        Color color = new Color(PreviewScreen.GetComponent<Image>().color.r, PreviewScreen.GetComponent<Image>().color.g, PreviewScreen.GetComponent<Image>().color.b, 255);
        PreviewScreen.GetComponent<Image>().color = color;
        PreviewScreen.SetActive(true);
    }

    public void BeginToHidePrevScreen()
    {
        CancelInvoke("BeginToHidePrevScreen");
        StartCoroutine(HidePrevScreen());
        Loom.QueueOnMainThread(() => {
            CameraScript.instanse.StartApp();
        }, .5f);
    }

    IEnumerator HidePrevScreen()
    {
        yield return new WaitForSeconds(.6f);
        for (float i = 0; i < 1.1; i += 0.025f)
        {
            float transparent = Mathf.Lerp(1, 0, i);
            Color color = new Color(PreviewScreen.GetComponent<Image>().color.r, PreviewScreen.GetComponent<Image>().color.g, PreviewScreen.GetComponent<Image>().color.b, transparent);
            PreviewScreen.GetComponent<Image>().color = color;
            yield return new WaitForSeconds(0.01f);
        }
        PreviewScreen.SetActive(false);
    }

    IEnumerator ShowPrevScreen()
    {
        PreviewScreen.SetActive(true);
        for (float i = 0; i < 1.1; i += 0.04f)
        {
            float transparent = Mathf.Lerp(0, 1, i);
            Color color = new Color(PreviewScreen.GetComponent<Image>().color.r, PreviewScreen.GetComponent<Image>().color.g, PreviewScreen.GetComponent<Image>().color.b, transparent);
            PreviewScreen.GetComponent<Image>().color = color;
            yield return new WaitForSeconds(0.01f);
        }
    }

}
